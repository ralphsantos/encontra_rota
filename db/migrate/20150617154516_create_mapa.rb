class CreateMapa < ActiveRecord::Migration
  def change
    create_table :mapas do |t|
      t.string :vertice
      t.string :vertice_destino
      t.int    :peso
    end
  end
end
